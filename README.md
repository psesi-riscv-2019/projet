# R5

This repository contain R5 source code. It's a 32 bit RISC-V processor with integer base only. There is not privilege support and not support for SYSTEM instructions, exceptions,interruptions.

The documentation with all the details will follow.


## `Core/`
`Core/` contain VHDL source code for R5.
You can build it with `make`

## `Simu/`
`Simu/` make a plateform to test R5, it's Core of R5 + "cache" to read from ELF. All of this is done with GHDL binding C (requiring ghdl with gcc backend).

`make` will produice `riscv_softsimu` which take as argument a RISCV ELF. Stack is at `0x10000000` (`Soft_SIMU/soft/mem.c`), Start adress is `0x08000000` . Simulation stop when Core ask for adress of label `_good`or `_bad`.  Exit status is 0 if simulation reach `_good`.

## `Synthese/`
This make synthesis of Core with [Alliance toolchain](https://www-soc.lip6.fr/equipe-cian/logiciels/alliance/).

## `riscv-tests/`
This directory is an adaptation of [riscv-tests](https://github.com/riscv/riscv-tests) to our plateform (mostly add `_good` and `_bad` label to stop simulation)

`make` will produice `../Simu/riscv_softsimu` ,build tests, and start `riscv-tests/launch-test.sh` which execute each test supported by R5.
