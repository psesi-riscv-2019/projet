package out_value is
  procedure good_end (adr : integer);
  attribute foreign of good_end : procedure is "VHPIDIRECT good_end";
  procedure bad_end (adr : integer);
  attribute foreign of bad_end : procedure is "VHPIDIRECT bad_end";
end out_value;

package body out_value is
  procedure good_end (adr : integer) is
  begin
    assert false severity failure;
  end good_end;
  procedure bad_end (adr : integer) is
  begin
    assert false severity failure;
  end bad_end;
end out_value;

