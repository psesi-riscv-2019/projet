rm -f riscv_core* fifo_* comparator* dec* exe* ifetch* mem* reg* shifter* wb* riscv_chip.vst *.ap *.vst


cp ../Core/riscv_core.vhdl .
cp ../Core/fifo_generic/fifo_generic.vhdl .
cp ../Core/comparator/comparator.vhdl .
cp ../Core/dec/dec.vhdl .
cp ../Core/exe/exe.vhdl .
cp ../Core/ifetch/ifetch.vhdl .
cp ../Core/mem/mem.vhdl .
cp ../Core/reg/reg.vhdl .
cp ../Core/shifter/shifter.vhdl .
cp ../Core/wb/wb.vhdl .

vasy -I vhdl -p -V -o -a -C 8 riscv_core riscv_core
boom -V -A -O riscv_core_model riscv_core_model
boog riscv_core_model riscv_core_model_boog
loon riscv_core_model_boog riscv_core_model

vasy -I vhdl -p -V -o -a -C 8 comparator comparator
boom -V -A -O comparator comparator
boog comparator comparator_boog
loon comparator_boog comparator

boom -V -A -O fifo_generic_fifo_dec_to_exe fifo_generic_fifo_dec_to_exe
boog fifo_generic_fifo_dec_to_exe fifo_generic_fifo_dec_to_exe_boog
loon fifo_generic_fifo_dec_to_exe_boog fifo_generic_fifo_dec_to_exe

boom -V -A -O fifo_generic_fifo_dec_to_ifetch fifo_generic_fifo_dec_to_ifetch
boog fifo_generic_fifo_dec_to_ifetch fifo_generic_fifo_dec_to_ifetch_boog
loon fifo_generic_fifo_dec_to_ifetch_boog fifo_generic_fifo_dec_to_ifetch

boom -V -A -O fifo_generic_fifo_exe_to_mem fifo_generic_fifo_exe_to_mem
boog fifo_generic_fifo_exe_to_mem fifo_generic_fifo_exe_to_mem_boog
loon fifo_generic_fifo_exe_to_mem_boog fifo_generic_fifo_exe_to_mem

boom -V -A -O fifo_generic_fifo_ifetch_to_dec fifo_generic_fifo_ifetch_to_dec
boog fifo_generic_fifo_ifetch_to_dec fifo_generic_fifo_ifetch_to_dec_boog
loon fifo_generic_fifo_ifetch_to_dec_boog fifo_generic_fifo_ifetch_to_dec

boom -V -A -O fifo_generic_fifo_mem_to_wb fifo_generic_fifo_mem_to_wb
boog fifo_generic_fifo_mem_to_wb fifo_generic_fifo_mem_to_wb_boog
loon fifo_generic_fifo_mem_to_wb_boog fifo_generic_fifo_mem_to_wb

boom -V -A -O dec_decode_stage_model dec_decode_stage_model
boog dec_decode_stage_model dec_decode_stage_model_boog
loon dec_decode_stage_model_boog dec_decode_stage_model

boom -V -A -O exe_execute_stage_model exe_execute_stage_model
boog exe_execute_stage_model exe_execute_stage_model_boog
loon exe_execute_stage_model_boog exe_execute_stage_model

boom -V -A -O ifetch_ifetch_stage ifetch_ifetch_stage
boog ifetch_ifetch_stage ifetch_ifetch_stage_boog
loon ifetch_ifetch_stage_boog ifetch_ifetch_stage

boom -V -A -O mem_memory_stage mem_memory_stage
boog mem_memory_stage mem_memory_stage_boog
loon mem_memory_stage_boog mem_memory_stage

boom -V -A -O reg_reg_i reg_reg_i
boog reg_reg_i reg_reg_i_boog
loon reg_reg_i_boog reg_reg_i

boom -V -A -O shifter_shifter_i shifter_shifter_i
boog shifter_shifter_i shifter_shifter_i_boog
loon shifter_shifter_i_boog shifter_shifter_i

boom -V -A -O wb_write_back_stage wb_write_back_stage
boog wb_write_back_stage wb_write_back_stage_boog
loon wb_write_back_stage_boog wb_write_back_stage

vasy -I vhdl -V -o -a -C 8 riscv_chip riscv_chip


echo "Now you can open cgt"
echo "'File->Open cell' name = 'riscv_chip'"
echo "Plugins->chip placement"
echo "P&R->Kite -route"
#cgt
./doChip.py -V --cell=riscv_chip
