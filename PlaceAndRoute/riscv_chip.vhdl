library ieee;
use ieee.std_logic_1164.all;

entity riscv_chip is
	port(
	-- Icache interface
			inst_adr			: out Std_Logic_Vector(31 downto 0);
			inst_req	: out Std_Logic;

			inst_in			: in Std_Logic_Vector(31 downto 0);
			inst_valid			: in Std_Logic;

	-- Dcache interface
			data_adr			: out Std_Logic_Vector(31 downto 0);
			data_load_w			: out Std_Logic;
			data_store_b			: out Std_Logic;
			data_store_h			: out Std_Logic;
			data_store_w			: out Std_Logic;

			data_out			: out Std_Logic_Vector(31 downto 0);
			data_in			: in Std_Logic_Vector(31 downto 0);
			data_valid			: in Std_Logic;


	-- global interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vddi				: in bit;
			vssi				: in bit;
			vdde				: in bit;
			vsse				: in bit);
end riscv_chip;


architecture struct OF riscv_chip is

component riscV_Core
  port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global Asynchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      -- Instruction Cache Interface
      inst_adr    : out std_logic_vector(31 downto 0);      -- Instruction address
      inst_req    : out std_logic;                          -- Instruction request
      inst_in     : in  std_logic_vector(31 downto 0);      -- Instruction given by ICache
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : out std_logic_vector(31 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(31 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(31 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic                           -- Data valide

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
	end component;

Component pi_px -- Plot d'entrée
port (
  pad : in Std_Logic;	-- pad
  t : out Std_Logic;	-- t
  ck : in Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;

Component po_px -- Plot de sortie
port (
  i : in Std_Logic;	-- i
  pad : out Std_Logic;	-- pad
  ck : in Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;

Component pck_px -- Plot d'horloge (permet de redistribuer l'horloge sur la couronne d'horloge)
port (
  pad : in Std_Logic;	-- pad
  ck : out Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;

Component pvddeck_px -- Plot d'alimentation VDD plot
port (
  cko : out Std_Logic ;	-- cko
  ck : in Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;

Component pvsseck_px -- Plot d'alimentation VSS plot
port (
  cko : out Std_Logic ;	-- cko
  ck : in Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;

Component pvddick_px -- Plot d'alimentation VDD coeur
port (
  cko : out Std_Logic ;	-- cko
  ck : in Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;

Component pvssick_px -- Plot d'alimentation VSS coeur
port (
  cko : out Std_Logic ;	-- cko
  ck : in Std_Logic;	-- ck
  vdde : in BIT;	-- vdde
  vddi : in BIT;	-- vddi
  vsse : in BIT;	-- vsse
  vssi : in BIT	-- vssi
  );
end Component;


	signal core_inst_adr			: Std_Logic_Vector(31 downto 0);
	signal core_inst_req	: Std_Logic;
	signal pad_inst_in			: Std_Logic_Vector(31 downto 0);
	signal pad_inst_valid			: Std_Logic;
	signal core_data_adr			: Std_Logic_Vector(31 downto 0);
	signal core_data_load_w			: Std_Logic;
	signal core_data_store_b			: Std_Logic;
	signal core_data_store_h			: Std_Logic;
	signal core_data_store_w			: Std_Logic;
	signal core_data_out			: Std_Logic_Vector(31 downto 0);
	signal pad_data_in			: Std_Logic_Vector(31 downto 0);
	signal pad_data_valid			: Std_Logic;
	signal pad_ck					: Std_Logic;
	signal pad_reset_n			: Std_Logic;

	signal ck_ring					: Std_Logic;

begin
  core_i : riscV_Core
  port map( -- Global Interface
      ck  => pad_ck,
      reset_n => pad_reset_n,
      VDD => vddi,
      VSS => vssi,

      -- Instruction Cache Interface
      inst_adr => core_inst_adr,
      inst_req => core_inst_req,
      inst_in  => pad_inst_in,
      inst_valid => pad_inst_valid,

      -- Data Cache Interface
      data_adr => core_data_adr,
      data_load_w => core_data_load_w,
      data_store_b => core_data_store_b,
      data_store_h => core_data_store_h,
      data_store_w => core_data_store_w,
      data_out => core_data_out,
      data_in => pad_data_in,
      data_valid => pad_data_valid

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );

	if_adr_0  : po_px port map (i => core_inst_adr(0 ), pad => inst_adr(0 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_1  : po_px port map (i => core_inst_adr(1 ), pad => inst_adr(1 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_2  : po_px port map (i => core_inst_adr(2 ), pad => inst_adr(2 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_3  : po_px port map (i => core_inst_adr(3 ), pad => inst_adr(3 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_4  : po_px port map (i => core_inst_adr(4 ), pad => inst_adr(4 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_5  : po_px port map (i => core_inst_adr(5 ), pad => inst_adr(5 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_6  : po_px port map (i => core_inst_adr(6 ), pad => inst_adr(6 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_7  : po_px port map (i => core_inst_adr(7 ), pad => inst_adr(7 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_8  : po_px port map (i => core_inst_adr(8 ), pad => inst_adr(8 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_9  : po_px port map (i => core_inst_adr(9 ), pad => inst_adr(9 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_10 : po_px port map (i => core_inst_adr(10), pad => inst_adr(10), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_11 : po_px port map (i => core_inst_adr(11), pad => inst_adr(11), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_12 : po_px port map (i => core_inst_adr(12), pad => inst_adr(12), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_13 : po_px port map (i => core_inst_adr(13), pad => inst_adr(13), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_14 : po_px port map (i => core_inst_adr(14), pad => inst_adr(14), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_15 : po_px port map (i => core_inst_adr(15), pad => inst_adr(15), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_16 : po_px port map (i => core_inst_adr(16), pad => inst_adr(16), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_17 : po_px port map (i => core_inst_adr(17), pad => inst_adr(17), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_18 : po_px port map (i => core_inst_adr(18), pad => inst_adr(18), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_19 : po_px port map (i => core_inst_adr(19), pad => inst_adr(19), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_20 : po_px port map (i => core_inst_adr(20), pad => inst_adr(20), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_21 : po_px port map (i => core_inst_adr(21), pad => inst_adr(21), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_22 : po_px port map (i => core_inst_adr(22), pad => inst_adr(22), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_23 : po_px port map (i => core_inst_adr(23), pad => inst_adr(23), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_24 : po_px port map (i => core_inst_adr(24), pad => inst_adr(24), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_25 : po_px port map (i => core_inst_adr(25), pad => inst_adr(25), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_26 : po_px port map (i => core_inst_adr(26), pad => inst_adr(26), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_27 : po_px port map (i => core_inst_adr(27), pad => inst_adr(27), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_28 : po_px port map (i => core_inst_adr(28), pad => inst_adr(28), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_29 : po_px port map (i => core_inst_adr(29), pad => inst_adr(29), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_30 : po_px port map (i => core_inst_adr(30), pad => inst_adr(30), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	if_adr_31 : po_px port map (i => core_inst_adr(31), pad => inst_adr(31), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 

	if_adr_valid_pad : po_px port map (i =>  core_inst_req, pad => inst_req, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 


			--ic_inst			=> pad_inst_in,
	ic_inst_0  : pi_px port map (t => pad_inst_in(0 ), pad => inst_in(0 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_1  : pi_px port map (t => pad_inst_in(1 ), pad => inst_in(1 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_2  : pi_px port map (t => pad_inst_in(2 ), pad => inst_in(2 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_3  : pi_px port map (t => pad_inst_in(3 ), pad => inst_in(3 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_4  : pi_px port map (t => pad_inst_in(4 ), pad => inst_in(4 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_5  : pi_px port map (t => pad_inst_in(5 ), pad => inst_in(5 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_6  : pi_px port map (t => pad_inst_in(6 ), pad => inst_in(6 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_7  : pi_px port map (t => pad_inst_in(7 ), pad => inst_in(7 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_8  : pi_px port map (t => pad_inst_in(8 ), pad => inst_in(8 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_9  : pi_px port map (t => pad_inst_in(9 ), pad => inst_in(9 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_10 : pi_px port map (t => pad_inst_in(10), pad => inst_in(10), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_11 : pi_px port map (t => pad_inst_in(11), pad => inst_in(11), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_12 : pi_px port map (t => pad_inst_in(12), pad => inst_in(12), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_13 : pi_px port map (t => pad_inst_in(13), pad => inst_in(13), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_14 : pi_px port map (t => pad_inst_in(14), pad => inst_in(14), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_15 : pi_px port map (t => pad_inst_in(15), pad => inst_in(15), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_16 : pi_px port map (t => pad_inst_in(16), pad => inst_in(16), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_17 : pi_px port map (t => pad_inst_in(17), pad => inst_in(17), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_18 : pi_px port map (t => pad_inst_in(18), pad => inst_in(18), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_19 : pi_px port map (t => pad_inst_in(19), pad => inst_in(19), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_20 : pi_px port map (t => pad_inst_in(20), pad => inst_in(20), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_21 : pi_px port map (t => pad_inst_in(21), pad => inst_in(21), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_22 : pi_px port map (t => pad_inst_in(22), pad => inst_in(22), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_23 : pi_px port map (t => pad_inst_in(23), pad => inst_in(23), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_24 : pi_px port map (t => pad_inst_in(24), pad => inst_in(24), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_25 : pi_px port map (t => pad_inst_in(25), pad => inst_in(25), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_26 : pi_px port map (t => pad_inst_in(26), pad => inst_in(26), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_27 : pi_px port map (t => pad_inst_in(27), pad => inst_in(27), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_28 : pi_px port map (t => pad_inst_in(28), pad => inst_in(28), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_29 : pi_px port map (t => pad_inst_in(29), pad => inst_in(29), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_30 : pi_px port map (t => pad_inst_in(30), pad => inst_in(30), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	ic_inst_31 : pi_px port map (t => pad_inst_in(31), pad => inst_in(31), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--ic_stall			=> pad_inst_valid,
	ic_stall_pad : pi_px port map (t => pad_inst_valid, pad => inst_valid, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--mem_adr			=> core_data_adr,
	mem_adr_0  : po_px port map (i => core_data_adr(0 ), pad => data_adr(0 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_1  : po_px port map (i => core_data_adr(1 ), pad => data_adr(1 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_2  : po_px port map (i => core_data_adr(2 ), pad => data_adr(2 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_3  : po_px port map (i => core_data_adr(3 ), pad => data_adr(3 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_4  : po_px port map (i => core_data_adr(4 ), pad => data_adr(4 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_5  : po_px port map (i => core_data_adr(5 ), pad => data_adr(5 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_6  : po_px port map (i => core_data_adr(6 ), pad => data_adr(6 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_7  : po_px port map (i => core_data_adr(7 ), pad => data_adr(7 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_8  : po_px port map (i => core_data_adr(8 ), pad => data_adr(8 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_9  : po_px port map (i => core_data_adr(9 ), pad => data_adr(9 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_10 : po_px port map (i => core_data_adr(10), pad => data_adr(10), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_11 : po_px port map (i => core_data_adr(11), pad => data_adr(11), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_12 : po_px port map (i => core_data_adr(12), pad => data_adr(12), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_13 : po_px port map (i => core_data_adr(13), pad => data_adr(13), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_14 : po_px port map (i => core_data_adr(14), pad => data_adr(14), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_15 : po_px port map (i => core_data_adr(15), pad => data_adr(15), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_16 : po_px port map (i => core_data_adr(16), pad => data_adr(16), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_17 : po_px port map (i => core_data_adr(17), pad => data_adr(17), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_18 : po_px port map (i => core_data_adr(18), pad => data_adr(18), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_19 : po_px port map (i => core_data_adr(19), pad => data_adr(19), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_20 : po_px port map (i => core_data_adr(20), pad => data_adr(20), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_21 : po_px port map (i => core_data_adr(21), pad => data_adr(21), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_22 : po_px port map (i => core_data_adr(22), pad => data_adr(22), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_23 : po_px port map (i => core_data_adr(23), pad => data_adr(23), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_24 : po_px port map (i => core_data_adr(24), pad => data_adr(24), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_25 : po_px port map (i => core_data_adr(25), pad => data_adr(25), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_26 : po_px port map (i => core_data_adr(26), pad => data_adr(26), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_27 : po_px port map (i => core_data_adr(27), pad => data_adr(27), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_28 : po_px port map (i => core_data_adr(28), pad => data_adr(28), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_29 : po_px port map (i => core_data_adr(29), pad => data_adr(29), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_30 : po_px port map (i => core_data_adr(30), pad => data_adr(30), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_adr_31 : po_px port map (i => core_data_adr(31), pad => data_adr(31), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--mem_stw			=> core_data_store_w,
	mem_stw_pad : po_px port map (i =>  core_data_store_w, pad => data_store_w, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--mem_sth			=> core_data_store_h,
	mem_sth_pad : po_px port map (i =>  core_data_store_h, pad => data_store_h, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--mem_stb			=> core_data_store_b,
	mem_stb_pad : po_px port map (i =>  core_data_store_b, pad => data_store_b, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--mem_load			=> core_data_load_w,
	mem_load_pad : po_px port map (i =>  core_data_load_w, pad => data_load_w, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--mem_data			=> core_data_out,
	mem_data_0  : po_px port map (i => core_data_out(0 ), pad => data_out(0 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_1  : po_px port map (i => core_data_out(1 ), pad => data_out(1 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_2  : po_px port map (i => core_data_out(2 ), pad => data_out(2 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_3  : po_px port map (i => core_data_out(3 ), pad => data_out(3 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_4  : po_px port map (i => core_data_out(4 ), pad => data_out(4 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_5  : po_px port map (i => core_data_out(5 ), pad => data_out(5 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_6  : po_px port map (i => core_data_out(6 ), pad => data_out(6 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_7  : po_px port map (i => core_data_out(7 ), pad => data_out(7 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_8  : po_px port map (i => core_data_out(8 ), pad => data_out(8 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_9  : po_px port map (i => core_data_out(9 ), pad => data_out(9 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_10 : po_px port map (i => core_data_out(10), pad => data_out(10), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_11 : po_px port map (i => core_data_out(11), pad => data_out(11), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_12 : po_px port map (i => core_data_out(12), pad => data_out(12), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_13 : po_px port map (i => core_data_out(13), pad => data_out(13), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_14 : po_px port map (i => core_data_out(14), pad => data_out(14), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_15 : po_px port map (i => core_data_out(15), pad => data_out(15), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_16 : po_px port map (i => core_data_out(16), pad => data_out(16), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_17 : po_px port map (i => core_data_out(17), pad => data_out(17), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_18 : po_px port map (i => core_data_out(18), pad => data_out(18), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_19 : po_px port map (i => core_data_out(19), pad => data_out(19), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_20 : po_px port map (i => core_data_out(20), pad => data_out(20), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_21 : po_px port map (i => core_data_out(21), pad => data_out(21), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_22 : po_px port map (i => core_data_out(22), pad => data_out(22), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_23 : po_px port map (i => core_data_out(23), pad => data_out(23), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_24 : po_px port map (i => core_data_out(24), pad => data_out(24), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_25 : po_px port map (i => core_data_out(25), pad => data_out(25), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_26 : po_px port map (i => core_data_out(26), pad => data_out(26), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_27 : po_px port map (i => core_data_out(27), pad => data_out(27), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_28 : po_px port map (i => core_data_out(28), pad => data_out(28), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_29 : po_px port map (i => core_data_out(29), pad => data_out(29), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_30 : po_px port map (i => core_data_out(30), pad => data_out(30), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	mem_data_31 : po_px port map (i => core_data_out(31), pad => data_out(31), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	--		dc_data			=> pad_data_in,
	dc_data_0  : pi_px port map (t => pad_data_in(0 ), pad => data_in(0 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_1  : pi_px port map (t => pad_data_in(1 ), pad => data_in(1 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_2  : pi_px port map (t => pad_data_in(2 ), pad => data_in(2 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_3  : pi_px port map (t => pad_data_in(3 ), pad => data_in(3 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_4  : pi_px port map (t => pad_data_in(4 ), pad => data_in(4 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_5  : pi_px port map (t => pad_data_in(5 ), pad => data_in(5 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_6  : pi_px port map (t => pad_data_in(6 ), pad => data_in(6 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_7  : pi_px port map (t => pad_data_in(7 ), pad => data_in(7 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_8  : pi_px port map (t => pad_data_in(8 ), pad => data_in(8 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_9  : pi_px port map (t => pad_data_in(9 ), pad => data_in(9 ), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_10 : pi_px port map (t => pad_data_in(10), pad => data_in(10), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_11 : pi_px port map (t => pad_data_in(11), pad => data_in(11), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_12 : pi_px port map (t => pad_data_in(12), pad => data_in(12), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_13 : pi_px port map (t => pad_data_in(13), pad => data_in(13), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_14 : pi_px port map (t => pad_data_in(14), pad => data_in(14), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_15 : pi_px port map (t => pad_data_in(15), pad => data_in(15), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_16 : pi_px port map (t => pad_data_in(16), pad => data_in(16), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_17 : pi_px port map (t => pad_data_in(17), pad => data_in(17), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_18 : pi_px port map (t => pad_data_in(18), pad => data_in(18), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_19 : pi_px port map (t => pad_data_in(19), pad => data_in(19), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_20 : pi_px port map (t => pad_data_in(20), pad => data_in(20), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_21 : pi_px port map (t => pad_data_in(21), pad => data_in(21), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_22 : pi_px port map (t => pad_data_in(22), pad => data_in(22), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_23 : pi_px port map (t => pad_data_in(23), pad => data_in(23), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_24 : pi_px port map (t => pad_data_in(24), pad => data_in(24), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_25 : pi_px port map (t => pad_data_in(25), pad => data_in(25), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_26 : pi_px port map (t => pad_data_in(26), pad => data_in(26), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_27 : pi_px port map (t => pad_data_in(27), pad => data_in(27), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_28 : pi_px port map (t => pad_data_in(28), pad => data_in(28), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_29 : pi_px port map (t => pad_data_in(29), pad => data_in(29), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_30 : pi_px port map (t => pad_data_in(30), pad => data_in(30), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	dc_data_31 : pi_px port map (t => pad_data_in(31), pad => data_in(31), ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	--		dc_stall			=> pad_data_valid,
	dc_stall_pad : pi_px port map (t => pad_data_valid, pad => data_valid, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--ck					=> pad_ck,
	pck : pck_px port map (pad => ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
			--reset_n			=> pad_reset_n,
	preset : pi_px port map (t => pad_reset_n, pad => reset_n, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 

	pvdde_0 : pvddeck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvdde_1 : pvddeck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvdde_2 : pvddeck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvdde_3 : pvddeck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvddi_0 : pvddick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvddi_1 : pvddick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvddi_2 : pvddick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvddi_3 : pvddick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 

	pvsse_0 : pvsseck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvsse_1 : pvsseck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvsse_2 : pvsseck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvsse_3 : pvsseck_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvssi_0 : pvssick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvssi_1 : pvssick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvssi_2 : pvssick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 
	pvssi_3 : pvssick_px port map (cko => pad_ck, ck => ck_ring, vdde => vdde, vddi => vddi, vsse => vsse, vssi => vssi); 

end;
