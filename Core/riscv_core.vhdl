library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity riscV_Core is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global synchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      -- Instruction Cache Interface
      inst_adr    : out std_logic_vector(A-1 downto 0);      -- Instruction address
      inst_req    : out std_logic;                          -- Instruction request
      inst_in     : in  std_logic_vector(D-1 downto 0);      -- Instruction given by ICache
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : out std_logic_vector(D-1 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(D-1 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(D-1 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic                           -- Data valide

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
end riscV_Core;

architecture Behavioral of riscV_Core is
component fifo_generic
	GENERIC( N : natural := 32);
	PORT(
		din		: in std_logic_vector(N-1 downto 0);
		dout		: out std_logic_vector(N-1 downto 0);

		-- commands
		push		: in std_logic;
		pop		: in std_logic;

		-- flags
		full		: out std_logic;
		empty		: out std_logic;

		reset_n	: in std_logic;
		ck			: in std_logic;
		vdd		: in bit;
		vss		: in bit
	);
END component;

component IFETCH
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
      ck          : in Std_Logic;
      reset_n     : in Std_Logic;
      vdd         : in bit;
      vss         : in bit;

      -- IFETCH to ICACHE Interface
      inst_adr    : out std_logic_vector(A-1 downto 0);      -- Instruction address
      inst_req    : out std_logic;                          -- Instruction request
      inst_in     : in  std_logic_vector(D-1 downto 0);      -- Instruction given by ICache
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- IFETCH from DECODE Interface
      NEXT_PC : in std_logic_vector(A-1 downto 0);

      -- IFETCH to DECODE Interface
      PC : out std_logic_vector(A-1 downto 0);
      INST : out std_logic_vector(D-1 downto 0);
      fetch_ok : out std_logic;

			-- FIFO IF_2_DEC
      if2dec_push:  out std_logic;
      if2dec_full : in std_logic;

			-- FIFO DEC_2_IF
      dec2if_pop: out std_logic;
      dec2if_empty: in std_logic

      -- Other
  );
end component;

component DEC
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;
			-- DECODE to IFETCH Interface
      NEXT_NEXT_PC : out std_logic_vector(A-1 downto 0);

			-- DECODE from IFETCH Interface
      PC : in std_logic_vector(A-1 downto 0);
      INST : in std_logic_vector(D-1 downto 0);
      fetch_ok : in std_logic;

			-- DECODE to EXECUTE Interface
      OP1 : out std_logic_vector(D-1 downto 0);
      OP2 : out std_logic_vector(D-1 downto 0);
      STORE : out std_logic_vector(D-1 downto 0);
      RD: out std_logic_vector(4 downto 0);

      BYPASS_1_A : out std_logic;
      BYPASS_2_A : out std_logic;
      BYPASS_STORE_A : out std_logic;

      INST_12 : out std_logic;
      INST_13 : out std_logic;
      INST_14 : out std_logic;
      INST_30 : out std_logic;
      SUB : out std_logic;

      ADD_SHIFT_or_OTHERS : out std_logic;
      ADD_or_SHIFT : out std_logic;

      MEM_W_NOT_R : out std_logic;
      MEM_OP : out std_logic;
      MEM_W : out std_logic;
      MEM_H_B : out std_logic;
      MEM_S_U : out std_logic;

      -- Registers Interface from WB
      RD_W: in std_logic_vector(4 downto 0);
      W_ENABLE: in std_logic;
      RES_W: in std_logic_vector(D-1 downto 0);

			-- FIFO DEC_2_EXE
      dec2exe_push:  out std_logic;
      dec2exe_full : in std_logic;

			-- FIFO IF_2_DEC
      if2dec_pop : out std_logic;
      if2dec_empty : in std_logic;

			-- FIFO DEC_2_IF
      dec2if_push:  out std_logic;
      dec2if_full : in std_logic;

      -- Other
      BYPASS_VAL : in std_logic_vector(D-1 downto 0)
		);
end component;

component EXE
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;

			-- EXECUTE from DECODE Interface
      OP1 : in std_logic_vector(D-1 downto 0);
      OP2 : in std_logic_vector(D-1 downto 0);
      STORE_IN : in std_logic_vector(D-1 downto 0);
      RD_IN: in std_logic_vector(4 downto 0);

      BYPASS_1_A : in std_logic;
      BYPASS_2_A : in std_logic;
      BYPASS_STORE_A : in std_logic;

      INST_12 : in std_logic;
      INST_13 : in std_logic;
      INST_14 : in std_logic;
      INST_30 : in std_logic;
      SUB : in std_logic;

      ADD_SHIFT_or_OTHERS : in std_logic;
      ADD_or_SHIFT : in std_logic;

      MEM_W_NOT_R_IN : in std_logic;
      MEM_OP_IN : in std_logic;
      MEM_W_IN : in std_logic;
      MEM_H_B_IN : in std_logic;
      MEM_S_U_IN : in std_logic;

			-- EXECUTE to MEMORY Interface
      RD_OUT : out std_logic_vector(4 downto 0);
      STORE_OUT : out std_logic_vector(D-1 downto 0);
      RES : out std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R_OUT : out std_logic;
      MEM_OP_OUT : out std_logic;
      MEM_W_OUT : out std_logic;
      MEM_H_B_OUT : out std_logic;
      MEM_S_U_OUT : out std_logic;

			-- FIFO EXE_2_MEM
      exe2mem_push:  out std_logic;
      exe2mem_full : in std_logic;

			-- FIFO DEC_2_EXE
      dec2exe_pop : out std_logic;
      dec2exe_empty : in std_logic;

			-- Other
      BYPASS_VAL : in std_logic_vector(D-1 downto 0)
		);
end component;

component MEM
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;

			-- MEMORY to DCACHE Interface
      data_adr    : out std_logic_vector(A-1 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(D-1 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(D-1 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic;                           -- Data valide

			-- MEMORY from EXECUTE Interface
      RD_IN : in std_logic_vector(4 downto 0);
      STORE : in std_logic_vector(D-1 downto 0);
      RES_IN : in std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R_IN : in std_logic;
      MEM_OP_IN : in std_logic;
      MEM_W_IN : in std_logic;
      MEM_H_B_IN : in std_logic;
      MEM_S_U_IN : in std_logic;

			-- MEMORY to WRITE_BACK Interface
      RD_OUT : out std_logic_vector(4 downto 0);
      RES_OUT : out std_logic_vector(D-1 downto 0);
      MEM_RES : out std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R_OUT : out std_logic;
      MEM_OP_OUT : out std_logic;
      MEM_W_OUT : out std_logic;
      MEM_H_B_OUT : out std_logic;
      MEM_S_U_OUT : out std_logic;

			-- FIFO MEM_2_WB
      mem2wb_push:  out std_logic;
      mem2wb_full : in std_logic;

			-- FIFO EXE_2_MEM
      exe2mem_pop : out std_logic;
      exe2mem_empty : in std_logic

			-- Other
		);
end component;

component WB
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;
			-- WRITE_BACK from MEMORY Interface
      RD_IN : in std_logic_vector(4 downto 0);
      RES : in std_logic_vector(D-1 downto 0);
      MEM_RES : in std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R : in std_logic;
      MEM_OP : in std_logic;
      MEM_W : in std_logic;
      MEM_H_B : in std_logic;
      MEM_S_U : in std_logic;

			-- WRITE_BACK to Registers Interface
      RD_OUT : out std_logic_vector(4 downto 0);
      W_ENABLE : out std_logic;
      RES_OUT : out std_logic_vector(D-1 downto 0);

			-- FIFO MEM_2_WB
      mem2wb_pop : out std_logic;
      mem2wb_empty : in std_logic

			-- Other
		);
end component;

signal  ifetch_NEXT_PC : std_logic_vector(A-1 downto 0);
signal  ifetch_PC  : std_logic_vector(A-1 downto 0);
signal  ifetch_INST  : std_logic_vector(D-1 downto 0);
signal  ifetch_fetch_ok : std_logic;
signal  if2dec_push : std_logic;
signal  if2dec_full : std_logic;
signal  dec2if_pop : std_logic;
signal  dec2if_empty : std_logic;

-- DEC
signal  dec_NEXT_PC  : std_logic_vector(A-1 downto 0);
signal  dec_PC  : std_logic_vector(A-1 downto 0);
signal  dec_INST  : std_logic_vector(D-1 downto 0);
signal  dec_OP1  : std_logic_vector(D-1 downto 0);
signal  dec_OP2  : std_logic_vector(D-1 downto 0);
signal  dec_STORE  : std_logic_vector(D-1 downto 0);
signal  dec_RD : std_logic_vector(4 downto 0);
signal  dec_BYPASS_1_A  : std_logic;
signal  dec_BYPASS_2_A  : std_logic;
signal  dec_BYPASS_STORE_A  : std_logic;
signal  dec_INST_12  : std_logic;
signal  dec_INST_13  : std_logic;
signal  dec_INST_14  : std_logic;
signal  dec_INST_30  : std_logic;
signal  dec_SUB  : std_logic;
signal  dec_ADD_SHIFT_or_OTHERS  : std_logic;
signal  dec_ADD_or_SHIFT  : std_logic;
signal  dec_MEM_W_NOT_R  : std_logic;
signal  dec_MEM_OP  : std_logic;
signal  dec_MEM_W : std_logic;
signal  dec_MEM_H_B : std_logic;
signal  dec_MEM_S_U : std_logic;
signal  RD_W  : std_logic_vector(4 downto 0);
signal  W_ENABLE  : std_logic;
signal  RES_OUT  : std_logic_vector(D-1 downto 0);
signal  BYPASS_VAL  : std_logic_vector(D-1 downto 0);
signal  dec2exe_push : std_logic;
signal  dec2exe_full  : std_logic;
signal  if2dec_pop  : std_logic;
signal  if2dec_empty  : std_logic;
signal  dec2if_push : std_logic;
signal  dec2if_full : std_logic;

-- EXE
signal  exe_OP1  : std_logic_vector(D-1 downto 0);
signal  exe_OP2  : std_logic_vector(D-1 downto 0);
signal  exe_STORE_IN  : std_logic_vector(D-1 downto 0);
signal  exe_RD_IN : std_logic_vector(4 downto 0);
signal  exe_BYPASS_1_A  : std_logic;
signal  exe_BYPASS_2_A  : std_logic;
signal  exe_BYPASS_STORE_A  : std_logic;
signal  exe_INST_12  : std_logic;
signal  exe_INST_13  : std_logic;
signal  exe_INST_14  : std_logic;
signal  exe_INST_30  : std_logic;
signal  exe_SUB  : std_logic;
signal  exe_ADD_SHIFT_or_OTHERS  : std_logic;
signal  exe_ADD_or_SHIFT  : std_logic;
signal  exe_MEM_W_NOT_R_IN  : std_logic;
signal  exe_MEM_OP_IN  : std_logic;
signal  exe_MEM_W_IN  : std_logic;
signal  exe_MEM_H_B_IN  : std_logic;
signal  exe_MEM_S_U_IN  : std_logic;
signal  exe_RD_OUT  : std_logic_vector(4 downto 0);
signal  exe_STORE_OUT : std_logic_vector(D-1 downto 0);
signal  exe_RES  : std_logic_vector(D-1 downto 0);
signal  exe_MEM_W_NOT_R_OUT  : std_logic;
signal  exe_MEM_OP_OUT  : std_logic;
signal  exe_MEM_W_OUT  : std_logic;
signal  exe_MEM_H_B_OUT  : std_logic;
signal  exe_MEM_S_U_OUT  : std_logic;
signal  exe2mem_push : std_logic;
signal  exe2mem_full  : std_logic;
signal  dec2exe_pop  : std_logic;
signal  dec2exe_empty : std_logic;

-- MEM
signal  mem_RD_IN  : std_logic_vector(4 downto 0);
signal  mem_STORE : std_logic_vector(D-1 downto 0);
signal  mem_RES_IN  : std_logic_vector(D-1 downto 0);
signal  mem_MEM_W_NOT_R_IN  : std_logic;
signal  mem_MEM_OP_IN  : std_logic;
signal  mem_MEM_W_IN  : std_logic;
signal  mem_MEM_H_B_IN  : std_logic;
signal  mem_MEM_S_U_IN  : std_logic;
signal  mem_RD_OUT  : std_logic_vector(4 downto 0);
signal  mem_RES_OUT  : std_logic_vector(D-1 downto 0);
signal  mem_MEM_RES  : std_logic_vector(D-1 downto 0);
signal  mem_MEM_W_NOT_R_OUT  : std_logic;
signal  mem_MEM_OP_OUT  : std_logic;
signal  mem_MEM_W_OUT  : std_logic;
signal  mem_MEM_H_B_OUT  : std_logic;
signal  mem_MEM_S_U_OUT  : std_logic;
signal  mem2wb_push : std_logic;
signal  mem2wb_full  : std_logic;
signal  exe2mem_pop  : std_logic;
signal  exe2mem_empty : std_logic;

-- WB
signal  wb_RD_IN  : std_logic_vector(4 downto 0);
signal  wb_RES  : std_logic_vector(D-1 downto 0);
signal  wb_MEM_RES  : std_logic_vector(D-1 downto 0);
signal  wb_MEM_W_NOT_R  : std_logic;
signal  wb_MEM_OP  : std_logic;
signal  wb_MEM_W  : std_logic;
signal  wb_MEM_H_B  : std_logic;
signal  wb_MEM_S_U  : std_logic;
signal  mem2wb_pop  : std_logic;
signal  mem2wb_empty : std_logic;

begin

IFETCH_STAGE : IFETCH
  generic map(32, 32)
  port map( -- Global Interface
      ck				=> ck,
      reset_n		=> reset_n,
      vdd				=> vdd,
      vss				=> vss,

      -- IFETCH to ICACHE Interface
      inst_adr     => inst_adr,
      inst_req     => inst_req,
      inst_in      => inst_in,
      inst_valid   => inst_valid,

      -- IFETCH from DECODE Interface
      NEXT_PC => ifetch_NEXT_PC,
            -- IFETCH to DECODE Interface
      PC  => ifetch_PC,
      INST  => ifetch_INST,
      fetch_ok => ifetch_fetch_ok,

      -- FIFO IF_2_DEC
      if2dec_push => if2dec_push,
      if2dec_full  => if2dec_full,

			-- FIFO DEC_2_IF
      dec2if_pop  => dec2if_pop,
      dec2if_empty  => dec2if_empty

      -- Other
    );

FIFO_DEC_TO_IFETCH: FIFO_GENERIC
  generic map(32)
  port map(
    din => dec_NEXT_PC,
    dout => ifetch_NEXT_PC,
		push => dec2if_push,
		pop => dec2if_pop,

		-- flags
		full => dec2if_full,
		empty => dec2if_empty,

		reset_n => reset_n,
		ck => ck,
		vdd => vdd,
		vss => vss
  );

FIFO_IFETCH_TO_DEC: FIFO_GENERIC
  generic map(64)
  port map(
    din(63 downto 32) => ifetch_PC,
    din(31 downto 0) => ifetch_INST,
    dout(63 downto 32) => dec_PC,
    dout(31 downto 0) => dec_INST,
		push => if2dec_push,
		pop => if2dec_pop,

		-- flags
		full => if2dec_full,
		empty => if2dec_empty,

		reset_n => reset_n,
		ck => ck,
		vdd => vdd,
		vss => vss
  );

DECODE_STAGE : DEC
  generic map(32, 32)
  port map( -- Global Interface
      ck					=> ck,
      reset_n			=> reset_n,
      vdd				 	=> vdd,
      vss				 	=> vss,

            -- DECODE to IFETCH Interface
      NEXT_NEXT_PC  => dec_NEXT_PC,

            -- DECODE from IFETCH Interface
      PC  => dec_PC,
      INST  => dec_INST,
      fetch_ok => ifetch_fetch_ok,

            -- DECODE to EXECUTE Interface
      OP1  => dec_OP1,
      OP2  => dec_OP2,
      STORE  => dec_STORE,
      RD => dec_RD,

      BYPASS_1_A  => dec_BYPASS_1_A,
      BYPASS_2_A  => dec_BYPASS_2_A,
      BYPASS_STORE_A  => dec_BYPASS_STORE_A,

      INST_12  => dec_INST_12,
      INST_13  => dec_INST_13,
      INST_14  => dec_INST_14,
      INST_30  => dec_INST_30,
      SUB  => dec_SUB,

      ADD_SHIFT_or_OTHERS  => dec_ADD_SHIFT_or_OTHERS,
      ADD_or_SHIFT  => dec_ADD_or_SHIFT,

      MEM_W_NOT_R  => dec_MEM_W_NOT_R,
      MEM_OP  => dec_MEM_OP,
      MEM_W => dec_MEM_W,
      MEM_H_B => dec_MEM_H_B,
      MEM_S_U => dec_MEM_S_U,

      -- Registers Interface from WB
      RD_W  => RD_W,
      W_ENABLE  => W_ENABLE,
      RES_W  => RES_OUT,

      -- FIFO DEC_2_EXE
      dec2exe_push 	=> dec2exe_push,
      dec2exe_full  => dec2exe_full,

			-- FIFO IF_2_DEC
			if2dec_pop  	=> if2dec_pop,
      if2dec_empty  => if2dec_empty,

			-- FIFO DEC_2_IF
      dec2if_push => dec2if_push,
      dec2if_full => dec2if_full,

      -- Other
      BYPASS_VAL  => BYPASS_VAL
  );

FIFO_DEC_TO_EXE: FIFO_GENERIC
  generic map(116)
  port map(
    din(115 downto 84) => dec_OP1,
    din(83 downto 52) => dec_OP2,
    din(51 downto 20) => dec_STORE,
    din(19 downto 15) => dec_RD,
    din(14) => dec_BYPASS_STORE_A,
    din(13) => dec_BYPASS_1_A,
    din(12) => dec_BYPASS_2_A,
    din(11) => dec_INST_12,
    din(10) => dec_INST_13,
    din(9) => dec_INST_14,
    din(8) => dec_INST_30,
    din(7) => dec_SUB,
    din(6) => dec_ADD_SHIFT_or_OTHERS,
    din(5) => dec_ADD_or_SHIFT,
    din(4) => dec_MEM_W_NOT_R,
    din(3) => dec_MEM_OP,
    din(2) => dec_MEM_W,
    din(1) => dec_MEM_H_B,
    din(0) => dec_MEM_S_U,

    dout(115 downto 84) => exe_OP1,
    dout(83 downto 52) => exe_OP2,
    dout(51 downto 20) => exe_STORE_IN,
    dout(19 downto 15) => exe_RD_IN,
    dout(14) => exe_BYPASS_STORE_A,
    dout(13) => exe_BYPASS_1_A,
    dout(12) => exe_BYPASS_2_A,
    dout(11) => exe_INST_12,
    dout(10) => exe_INST_13,
    dout(9) => exe_INST_14,
    dout(8) => exe_INST_30,
    dout(7) => exe_SUB,
    dout(6) => exe_ADD_SHIFT_or_OTHERS,
    dout(5) => exe_ADD_or_SHIFT,
    dout(4) => exe_MEM_W_NOT_R_IN,
    dout(3) => exe_MEM_OP_IN,
    dout(2) => exe_MEM_W_IN,
    dout(1) => exe_MEM_H_B_IN,
    dout(0) => exe_MEM_S_U_IN,

		push => dec2exe_push,
		pop => dec2exe_pop,
		-- flags
		full => dec2exe_full,
		empty => dec2exe_empty,

		reset_n => reset_n,
		ck => ck,
		vdd => vdd,
		vss => vss
  );

EXECUTE_STAGE : EXE
  generic map(32, 32)
  port map( -- Global Interface
      ck					 => ck					,
      reset_n			 => reset_n			,
      vdd				 => vdd				,
      vss				 => vss				,

			-- EXECUTE from DECODE Interface
      OP1  => exe_OP1 ,
      OP2  => exe_OP2 ,
      STORE_IN  => exe_STORE_IN ,
      RD_IN => exe_RD_IN,

      BYPASS_1_A  => exe_BYPASS_1_A ,
      BYPASS_2_A  => exe_BYPASS_2_A ,
      BYPASS_STORE_A  => exe_BYPASS_STORE_A ,

      INST_12  => exe_INST_12 ,
      INST_13  => exe_INST_13 ,
      INST_14  => exe_INST_14 ,
      INST_30  => exe_INST_30 ,
      SUB  => exe_SUB ,

      ADD_SHIFT_or_OTHERS  => exe_ADD_SHIFT_or_OTHERS ,
      ADD_or_SHIFT  => exe_ADD_or_SHIFT ,

      MEM_W_NOT_R_IN  => exe_MEM_W_NOT_R_IN ,
      MEM_OP_IN  => exe_MEM_OP_IN ,
      MEM_W_IN  => exe_MEM_W_IN ,
      MEM_H_B_IN  => exe_MEM_H_B_IN ,
      MEM_S_U_IN  => exe_MEM_S_U_IN ,

      -- EXECUTE to MEMORY Interface
      RD_OUT  => exe_RD_OUT ,
      STORE_OUT => exe_STORE_OUT,
      RES  => exe_RES ,
      MEM_W_NOT_R_OUT  => exe_MEM_W_NOT_R_OUT ,
      MEM_OP_OUT  => exe_MEM_OP_OUT ,
      MEM_W_OUT  => exe_MEM_W_OUT ,
      MEM_H_B_OUT  => exe_MEM_H_B_OUT ,
      MEM_S_U_OUT  => exe_MEM_S_U_OUT ,

      -- FIFO EXE_2_MEM
      exe2mem_push => exe2mem_push,
      exe2mem_full  => exe2mem_full ,

			-- FIFO DEC_2_EXE
      dec2exe_pop  => dec2exe_pop ,
      dec2exe_empty  => dec2exe_empty,
			-- Other
      BYPASS_VAL => BYPASS_VAL
          );
FIFO_EXE_TO_MEM: FIFO_GENERIC
  generic map(74)
  port map(
    din(73 downto 69) => exe_RD_OUT,
    din(68 downto 37) => exe_STORE_OUT,
    din(36 downto 5) => exe_RES,
    din(4) => exe_MEM_W_NOT_R_OUT,
    din(3) => exe_MEM_OP_OUT,
    din(2) => exe_MEM_W_OUT,
    din(1) => exe_MEM_H_B_OUT,
    din(0) => exe_MEM_S_U_OUT,

    dout(73 downto 69) => mem_RD_IN,
    dout(68 downto 37) => mem_STORE,
    dout(36 downto 5) => mem_RES_IN,
    dout(4) => mem_MEM_W_NOT_R_IN,
    dout(3) => mem_MEM_OP_IN,
    dout(2) => mem_MEM_W_IN,
    dout(1) => mem_MEM_H_B_IN,
    dout(0) => mem_MEM_S_U_IN,

		push => exe2mem_push,
		pop => exe2mem_pop,
		-- flags
		full => exe2mem_full,
		empty => exe2mem_empty,

		reset_n => reset_n,
		ck => ck,
		vdd => vdd,
		vss => vss
  );

BYPASS_VAL <= mem_RES_IN;

MEMORY_STAGE : MEM
  generic map(32, 32)
  port map( -- Global Interface
      ck					 => ck					,
      reset_n			 => reset_n			,
      vdd				 => vdd				,
      vss				 => vss				,

			-- MEMORY to DCACHE Interface
      data_adr     => data_adr    ,
      data_load_w => data_load_w,
      data_store_b => data_store_b,
      data_store_h => data_store_h,
      data_store_w => data_store_w,
      data_out     => data_out    ,
      data_in      => data_in     ,
      data_valid   => data_valid  ,

            -- MEMORY from EXECUTE Interface
      RD_IN  => mem_RD_IN ,
      STORE  => mem_STORE ,
      RES_IN  => mem_RES_IN ,
      MEM_W_NOT_R_IN  => mem_MEM_W_NOT_R_IN ,
      MEM_OP_IN  => mem_MEM_OP_IN ,
      MEM_W_IN  => mem_MEM_W_IN ,
      MEM_H_B_IN  => mem_MEM_H_B_IN ,
      MEM_S_U_IN  => mem_MEM_S_U_IN ,

            -- MEMORY to WRITE_BACK Interface
      RD_OUT  => mem_RD_OUT ,
      RES_OUT  => mem_RES_OUT ,
      MEM_RES  => mem_MEM_RES ,
      MEM_W_NOT_R_OUT  => mem_MEM_W_NOT_R_OUT ,
      MEM_OP_OUT  => mem_MEM_OP_OUT ,
      MEM_W_OUT  => mem_MEM_W_OUT ,
      MEM_H_B_OUT  => mem_MEM_H_B_OUT ,
      MEM_S_U_OUT  => mem_MEM_S_U_OUT ,

      -- FIFO MEM_2_WB
      mem2wb_push => mem2wb_push,
      mem2wb_full  => mem2wb_full ,

			-- FIFO EXE_2_MEM
      exe2mem_pop  => exe2mem_pop ,
      exe2mem_empty  => exe2mem_empty

      -- Other
          );

FIFO_MEM_TO_WB: FIFO_GENERIC
  generic map(74)
  port map(
    din(73 downto 69) => mem_RD_OUT,
    din(68 downto 37) => mem_MEM_RES,
    din(36 downto 5) => mem_RES_OUT,
    din(4) => mem_MEM_W_NOT_R_OUT,
    din(3) => mem_MEM_OP_OUT,
    din(2) => mem_MEM_W_OUT,
    din(1) => mem_MEM_H_B_OUT,
    din(0) => mem_MEM_S_U_OUT,

    dout(73 downto 69) => wb_RD_IN,
    dout(68 downto 37) => wb_MEM_RES,
    dout(36 downto 5) => wb_RES,
    dout(4) => wb_MEM_W_NOT_R,
    dout(3) => wb_MEM_OP,
    dout(2) => wb_MEM_W,
    dout(1) => wb_MEM_H_B,
    dout(0) => wb_MEM_S_U,

		push => mem2wb_push,
		pop => mem2wb_pop,
		-- flags
		full => mem2wb_full,
		empty => mem2wb_empty,

		reset_n => reset_n,
		ck => ck,
		vdd => vdd,
		vss => vss
  );

WRITE_BACK_STAGE : WB
  generic map(32, 32)
  port map( -- Global Interface
      ck					 => ck					,
      reset_n			 => reset_n			,
      vdd				 => vdd				,
      vss				 => vss				,

      -- WRITE_BACK from MEMORY Interface
      RD_IN  => wb_RD_IN ,
      RES  => wb_RES ,
      MEM_RES  => wb_MEM_RES ,
      MEM_W_NOT_R  => wb_MEM_W_NOT_R ,
      MEM_OP  => wb_MEM_OP ,
      MEM_W  => wb_MEM_W ,
      MEM_H_B  => wb_MEM_H_B ,
      MEM_S_U  => wb_MEM_S_U ,

            -- WRITE_BACK to Registers Interface
      RD_OUT  => RD_W,
      W_ENABLE  => W_ENABLE ,
      RES_OUT  => RES_OUT ,

      -- FIFO MEM_2_WB
      mem2wb_pop  => mem2wb_pop ,
      mem2wb_empty  => mem2wb_empty

			-- Other
          );
end Behavioral;
