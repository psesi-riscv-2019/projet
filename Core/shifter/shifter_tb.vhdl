library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

--on ecrit end entity a la fin et pas end avec le nom de l'entite
entity shifter_tb is
	generic(D : natural := 32); -- Data length
end entity shifter_tb;

-- pas besoin de preciser in/out
architecture arch of shifter_tb is
	--generic(D : natural := 32); -- Data length
	signal	a : Std_Logic_Vector(D-1 downto 0);



	signal	shamt : Std_Logic_Vector(4 downto 0); -- immediate active.
	signal	INST_14 : Std_Logic; 
	signal	INST_30 : Std_Logic;
	signal	RES : Std_Logic_Vector(D-1 downto 0);

	-- global terface
	signal	vdd : bit;
	signal	vss : bit;

begin
	-- Instanciation
	instance: entity work.shifter
	port map(
	a,
	shamt, 
	INST_14, 
	INST_30,
	RES, 
	vdd, 
	vss);

	process
		variable result: std_logic_vector(31 downto 0);
	begin
		a <= std_logic_vector(to_unsigned(42, a'length));

		shamt <= std_logic_vector(to_unsigned(3, shamt'length));

		INST_14 <= '1';
		INST_30 <= '0';

		--result := std_logic_vector(shift_left(to_unsigned(42, result'length), 3));
		result := std_logic_vector(shift_right(to_unsigned(42, result'length), 3));

		wait for 20 ns; 

		assert (result = RES)
		report "Addition Error:"
		& integer'image(to_integer(unsigned(result))) & " = "
		& integer'image(2) & " sll " & integer'image(1)
		& " = " & integer'image(to_integer(unsigned(RES)))
		severity error;

		assert false report "end of test" severity note;
		wait;
	end process;
end arch;
