library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reverser is
	port ( input : in std_logic_vector (31 downto 0);
	       reverse : in std_logic;
	       output : out std_logic_vector (31 downto 0));
end reverser;

architecture behavioral of reverser is
	signal temp: std_logic_vector (31 downto 0);
begin

--gen: 
	--process(input)
	--begin
		--for i in 0 to 31 loop
			--temp(i) <= input(31-i);
		--end loop;
	--end process;

temp( 0) <= input(31); temp( 1) <= input(30); temp( 2) <= input(29); temp( 3) <= input(28);
temp( 4) <= input(27); temp( 5) <= input(26); temp( 6) <= input(25); temp( 7) <= input(24);
temp( 8) <= input(23); temp( 9) <= input(22); temp(10) <= input(21); temp(11) <= input(20);
temp(12) <= input(19); temp(13) <= input(18); temp(14) <= input(17); temp(15) <= input(16);
temp(16) <= input(15); temp(17) <= input(14); temp(18) <= input(13); temp(19) <= input(12);
temp(20) <= input(11); temp(21) <= input(10); temp(22) <= input( 9); temp(23) <= input( 8);
temp(24) <= input( 7); temp(25) <= input( 6); temp(26) <= input( 5); temp(27) <= input( 4);
temp(28) <= input( 3); temp(29) <= input( 2); temp(30) <= input( 1); temp(31) <= input( 0);

	output <= temp when reverse='1' else input;
end behavioral;
