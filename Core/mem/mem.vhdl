library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity MEM is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;

			-- MEMORY to DCACHE Interface
      data_adr    : out std_logic_vector(A-1 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(D-1 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(D-1 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic;                           -- Data valide

			-- MEMORY from EXECUTE Interface
      RD_IN : in std_logic_vector(4 downto 0);
      STORE : in std_logic_vector(D-1 downto 0);
      RES_IN : in std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R_IN : in std_logic;
      MEM_OP_IN : in std_logic;
      MEM_W_IN : in std_logic;
      MEM_H_B_IN : in std_logic;
      MEM_S_U_IN : in std_logic;

			-- MEMORY to WRITE_BACK Interface
      RD_OUT : out std_logic_vector(4 downto 0);
      RES_OUT : out std_logic_vector(D-1 downto 0);
      MEM_RES : out std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R_OUT : out std_logic;
      MEM_OP_OUT : out std_logic;
      MEM_W_OUT : out std_logic;
      MEM_H_B_OUT : out std_logic;
      MEM_S_U_OUT : out std_logic;

			-- FIFO MEM_2_WB
      mem2wb_push:  out std_logic;
      mem2wb_full : in std_logic;

			-- FIFO EXE_2_MEM
      exe2mem_pop : out std_logic;
      exe2mem_empty : in std_logic

			-- Other
		);
end MEM;

architecture Behavioral of MEM is
begin
      data_adr <= RES_IN(D-1 downto 2) & "00" when MEM_W_NOT_R_IN = '0' else RES_IN;

      data_load_w <= '1' when MEM_OP_IN = '1' and exe2mem_empty = '0' and MEM_W_NOT_R_IN = '0' else '0'; 
      data_store_b <= '1' when MEM_OP_IN = '1' and exe2mem_empty = '0' and MEM_W_NOT_R_IN = '1' and MEM_H_B_IN = '0' else '0';
      data_store_h <= '1' when MEM_OP_IN = '1' and exe2mem_empty = '0' and MEM_W_NOT_R_IN = '1' and MEM_H_B_IN = '1' else '0';
      data_store_w <= '1' when MEM_OP_IN = '1' and exe2mem_empty = '0' and MEM_W_NOT_R_IN = '1' and MEM_W_IN = '1' else '0';

      data_out <= STORE;

      RD_OUT <= RD_IN;
      RES_OUT <= RES_IN;
      MEM_RES <= data_in;
      MEM_W_NOT_R_OUT <= MEM_W_NOT_R_IN ;
      MEM_OP_OUT <= MEM_OP_IN;
      MEM_W_OUT <= MEM_W_IN;
      MEM_H_B_OUT <= MEM_H_B_IN;
      MEM_S_U_OUT <= MEM_S_U_IN;

      mem2wb_push <= '1' when exe2mem_empty = '0' and ((data_valid = '1' and MEM_OP_IN = '1') or (MEM_OP_IN = '0')) else '0';

      exe2mem_pop <= '1' when exe2mem_empty = '0' and ((data_valid = '1' and MEM_OP_IN = '1') or (MEM_OP_IN = '0')) else '0';


end Behavioral;
