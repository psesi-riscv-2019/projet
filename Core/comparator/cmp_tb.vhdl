LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity cmp_tb is
end cmp_tb;


architecture cmp_tb_arch of cmp_tb is
 signal op1,op2: std_logic_vector(31 downto 0);
 signal res, unsigned: std_logic;
 signal vdd,vss : bit;

      function to_bstring(sl : std_logic) return string is
        variable sl_str_v : string(1 to 3);  -- std_logic image with quotes around
        begin
          sl_str_v := std_logic'image(sl);
          return "" & sl_str_v(2);  -- "" & character to get string
        end function;

      function to_bstring(slv : std_logic_vector) return string is
          alias    slv_norm : std_logic_vector(1 to slv'length) is slv;
          variable sl_str_v : string(1 to 1);  -- String of std_logic
          variable res_v    : string(1 to slv'length);
        begin
          for idx in slv_norm'range loop
            sl_str_v := to_bstring(slv_norm(idx));
            res_v(idx) := sl_str_v(1);
          end loop;
          return res_v;
      end function;

begin
  cmp_instance : entity work.comparator
  port map (
      a => op1,
      b => op2,
      unsigned => unsigned,
      res => res,
      vdd => vdd,
      vss => vss
  );
  process
  variable op1_r, op2_r : std_logic_vector(31 downto 0);
  variable unsigned_r : std_logic;
  begin
    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "00000000000000000000000000000000";
    op2 <= "11111111111111111111111111111111";
    unsigned <= '1';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 1?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "00000000000000000000000000000001";
    op2 <= "11111111111111111111111111111110";
    unsigned <= '1';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 1?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111111";
    op2 <= "11111111111111111111111111111111";
    unsigned <= '1';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111111";
    op2 <= "00000000000000000000000000000000";
    unsigned <= '1';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "signed : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111110";
    op2 <= "11111111111111111111111111111100";
    unsigned <= '1';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111101";
    op2 <= "11111111111111111111111111111111";
    unsigned <= '1';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 1?"severity note;

    REPORT "-------------------------------------------TIME to SIGNED------------------" severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "00000000000000000000000000000000";
    op2 <= "11111111111111111111111111111111";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "00000000000000000000000000000001";
    op2 <= "11111111111111111111111111111110";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111111";
    op2 <= "11111111111111111111111111111111";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111111";
    op2 <= "00000000000000000000000000000000";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 1?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111110";
    op2 <= "11111111111111111111111111111100";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111101";
    op2 <= "11111111111111111111111111111111";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 1?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "11111111111111111111111111111111";
    op2 <= "11111111111111111111111111111101";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "00000000000000000000000000000010";
    op2 <= "00000000000000000000000000000000";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 0?"severity note;

    REPORT "---------- a < b ? -------------" severity note;
    op1 <= "00000000000000000000000000000000";
    op2 <= "00000000000000000000000000000010";
    unsigned <= '0';
    wait for 20 ns;
    REPORT "op1 " & to_bstring(op1) severity note;
    REPORT "op2 " & to_bstring(op2) severity note;
    REPORT "unsigned : " & std_logic'image(unsigned) severity note;
    REPORT "res " & std_logic'image(res) & " = 1?"severity note;

    wait; -- Stop process ?
  end process;
end cmp_tb_arch;

