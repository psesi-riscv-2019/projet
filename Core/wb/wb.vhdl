library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity WB is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;
			-- WRITE_BACK from MEMORY Interface
      RD_IN : in std_logic_vector(4 downto 0);
      RES : in std_logic_vector(D-1 downto 0);
      MEM_RES : in std_logic_vector(D-1 downto 0);
      MEM_W_NOT_R : in std_logic;
      MEM_OP : in std_logic;
      MEM_W : in std_logic;
      MEM_H_B : in std_logic;
      MEM_S_U : in std_logic;

			-- WRITE_BACK to Registers Interface
      RD_OUT : out std_logic_vector(4 downto 0);
      W_ENABLE : out std_logic;
      RES_OUT : out std_logic_vector(D-1 downto 0);

			-- FIFO MEM_2_WB
      mem2wb_pop : out std_logic;
      mem2wb_empty : in std_logic

			-- Other
		);
end WB;

architecture Behavioral of WB is

signal dmem_bytes_selected : std_logic_vector(D-1 downto 0);
signal dmem_signed_unsigned : std_logic_vector(D-1 downto 0);
signal dmem_loaded : std_logic_vector(D-1 downto 0);
signal sign : std_logic;
signal size_and_sign : std_logic_vector(1 downto 0);
signal res_1_0_and_size : std_logic_vector(2 downto 0);
begin

      res_1_0_and_size <= RES(1 downto 0) & MEM_H_B;
      with res_1_0_and_size select
        dmem_bytes_selected <=	X"000000" & MEM_RES(31 downto 24)	when "110", --lb
            X"000000" & MEM_RES(23 downto 16)	when "100", --lb
            X"000000" & MEM_RES(15 downto 8)		when "010", --lb
            X"000000" & MEM_RES(7 downto 0)		when "000", --lb

            X"0000" & MEM_RES(31 downto 16)		when "101", --lh
            X"0000" & MEM_RES(15 downto 0)		when others; --"001"; --lh

      sign <= '0' when MEM_S_U = '1' else
              dmem_bytes_selected(15) when MEM_H_B = '1' and MEM_S_U = '0' else
              dmem_bytes_selected(7); -- MEM_H_B = '0' and MEM_S_U = '0'

      dmem_signed_unsigned <=
          sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & dmem_bytes_selected(15 downto 0) when MEM_H_B = '1' --half
          else
          sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & sign & dmem_bytes_selected(7 downto 0); --byte

      dmem_loaded <= MEM_RES when MEM_W = '1' else dmem_signed_unsigned;


      RD_OUT <= RD_IN;
      W_ENABLE <= '1' when mem2wb_empty = '0' and MEM_W_NOT_R /= '1' else '0'; -- When store, dont do write in register
      RES_OUT <= dmem_loaded when MEM_OP = '1' else RES;

      mem2wb_pop <= '1' when mem2wb_empty = '0' else '0';

end Behavioral;
