library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity DEC is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;
			-- DECODE to IFETCH Interface
      NEXT_NEXT_PC : out std_logic_vector(A-1 downto 0);

			-- DECODE from IFETCH Interface
      PC : in std_logic_vector(A-1 downto 0);
      INST : in std_logic_vector(D-1 downto 0);
      fetch_ok : in std_logic;

			-- DECODE to EXECUTE Interface
      OP1 : out std_logic_vector(D-1 downto 0);
      OP2 : out std_logic_vector(D-1 downto 0);
      STORE : out std_logic_vector(D-1 downto 0);
      RD: out std_logic_vector(4 downto 0);

      BYPASS_1_A : out std_logic;
      BYPASS_2_A : out std_logic;
      BYPASS_STORE_A : out std_logic;

      INST_12 : out std_logic;
      INST_13 : out std_logic;
      INST_14 : out std_logic;
      INST_30 : out std_logic;
      SUB : out std_logic;

      ADD_SHIFT_or_OTHERS : out std_logic;
      ADD_or_SHIFT : out std_logic;

      MEM_W_NOT_R : out std_logic;
      MEM_OP : out std_logic;
      MEM_W : out std_logic;
      MEM_H_B : out std_logic;
      MEM_S_U : out std_logic;

      -- Registers Interface from WB
      RD_W: in std_logic_vector(4 downto 0);
      W_ENABLE: in std_logic;
      RES_W: in std_logic_vector(D-1 downto 0);

			-- FIFO DEC_2_EXE
      dec2exe_push:  out std_logic;
      dec2exe_full : in std_logic;

			-- FIFO IF_2_DEC
      if2dec_pop : out std_logic;
      if2dec_empty : in std_logic;

			-- FIFO DEC_2_IF
      dec2if_push:  out std_logic;
      dec2if_full : in std_logic;

      -- Other
      BYPASS_VAL : in std_logic_vector(D-1 downto 0)
		);
end DEC;

architecture Behavioral of DEC is

component comparator
  port(
    a       : in  Std_Logic_Vector(31 downto 0);
    b       : in  Std_Logic_Vector(31 downto 0);
    unsigned_s       : in  Std_Logic;
    res      : out Std_Logic;
    -- global interface
    vdd       : in  bit;
    vss       : in  bit );
end component;
component REG
  generic( D : natural := 32); -- Data length
  port(
      R1_ADR : in std_logic_vector(4 downto 0);
      R1_valid : out std_logic;
      REG_R1 : out std_logic_vector(D-1 downto 0);

      R2_ADR : in std_logic_vector(4 downto 0);
      R2_valid : out std_logic;
      REG_R2 : out std_logic_vector(D-1 downto 0);

      INVAL_ADR : in std_logic_vector(4 downto 0);
      INVAL_REQ : in std_logic;
      INVAL_VALIDITY : out std_logic;

      RD_W : in std_logic_vector(4 downto 0);
      W_ENABLE : in std_logic;
      RES_W : in std_logic_vector(D-1 downto 0);

      ck : in std_logic;
      reset_n : in std_logic;
      vdd : in bit;
      vss : in bit
  );
end component;

type dec_state is (RUN,BRANCH,FETCH,FETCH_START);
signal cur_state, next_state : dec_state;

signal OPCODE : std_logic_vector(6 downto 0);

signal I_type, S_type, B_type, U_type, J_type, R_type, FENCE_type : std_logic;
signal all_type : std_logic_vector(6 downto 0);
signal b_or_j : std_logic;
signal IMMEDIAT: std_logic_vector(D-1 downto 0);
signal MEM_OP_internal : std_logic;
signal operv : std_logic;
signal nop : std_logic;

signal NEXT_PC, PC_used, NEXT_NEXT_PC_internal, PC1, PC2, PC_added, PC_OFFSET, JR_OFFSET, JB_OFFSET : std_logic_vector(A-1 downto 0);
signal cmp_for_pc,cmp_lt, cmp_equal : std_logic;
signal PC_4_8 : std_logic_vector(A-1 downto 0);
signal next_4 : std_logic; -- If next_next_pc should be PC + 4
signal branch_taken : std_logic;


signal R1_ADR, R2_ADR, INVAL_ADR : std_logic_vector(4 downto 0);
signal  R1_VALID, R2_VALID, INVAL_REQ, INVAL_VALIDITY, inval_validity_reg : std_logic;
signal REG_R1,REG_R2 : std_logic_vector(D-1 downto 0);



signal OP1_BYPASSED: std_logic_vector(D-1 downto 0);
signal OP2_BYPASSED: std_logic_vector(D-1 downto 0);
signal BYPASS_1_B,BYPASS_2_B : std_logic; -- Bypass in dec
signal BYPASS_1_A_internal,BYPASS_2_A_internal, BYPASS_STORE_A_internal : std_logic; -- Bypass in exe

signal BYPASS_RD_ON_DEC_VALID, BYPASS_RD_WHEN_EXE_VALID : std_logic;
signal BYPASS_RD_ON_DEC, BYPASS_RD_WHEN_EXE : std_logic_vector(4 downto 0);

signal NEW_BYPASS_RD_VALID : std_logic;
signal NEW_BYPASS_RD : std_logic_vector(4 downto 0);


signal after_first_inst, after_first_inst_send : std_logic;
signal get_next_inst, pushed_to_exe, no_push_to_exe_require : std_logic;
signal dec2exe_push_internal : std_logic;
begin
    -- Global signals
      OPCODE <= INST(6 downto 0);
      -- INST type
      I_type <= '1' when OPCODE = "1100111" or OPCODE = "0010011" or OPCODE = "0000011" else '0';
      S_type <= '1' when OPCODE = "0100011" else '0';
      B_type <= '1' when OPCODE = "1100011" else '0';
      U_type <= '1' when OPCODE = "0010111" or OPCODE = "0110111" else '0';
      J_type <= '1' when OPCODE = "1101111" else '0';
      R_type <= '1' when OPCODE = "0110011" else '0';
      FENCE_type <= '1' when OPCODE = "0001111" else '0';
      all_type <= I_type & S_type & B_type & U_type & J_type & R_type & FENCE_type; -- We can assert this is not null
      B_or_J <= '1' when B_type = '1' or J_type = '1' or OPCODE = "1100111" else '0';
      nop <= '1' when FENCE_type = '1' and INST(14 downto 12) = "000" else '0';



    -- Immediate see figure 2.4 of spec 2.2
    with all_type select
      IMMEDIAT(0) <= INST(20) when "1000000", -- I type
                     INST(7) when "0100000", -- S type
                     '0' when others;

    with all_type select
      IMMEDIAT(4 downto 1) <= INST(24 downto 21) when "1000000" | "0000100" , -- I type + J type
                              INST(11 downto 8) when "0100000" | "0010000", -- S type + B type
                              "0000" when others;

      IMMEDIAT(10 downto 5) <= "000000" when U_type = '1' else INST(30 downto 25);

    with all_type select
      IMMEDIAT(11) <= INST(31) when "1000000" | "0100000" , -- I type + S type
                      INST(7) when "0010000", -- B type
                      '0' when "0001000", -- U type
                      INST(20) when others;

    IMMEDIAT(19 downto 12) <= INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) when I_type = '1' or S_type = '1' or B_type = '1' else INST(19 downto 12);

    with all_type select
      IMMEDIAT(30 downto 20) <= INST(30 downto 20) when "0001000", -- U type
                                INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) & INST(31) when others;

    IMMEDIAT(31) <= INST(31);

    -- Next Next PC
      JR_OFFSET <= std_logic_vector( signed(OP1_BYPASSED) + signed(IMMEDIAT));
      JB_OFFSET <= std_logic_vector( signed(PC_used) + signed(IMMEDIAT));
      PC_4_8 <=  X"00000004" when next_4 = '1' else  X"00000008";
      PC_used <=  NEXT_PC when next_4 = '1' else  PC; -- We use next_pc when we do +4 (after a branch/jump and at start)
      PC_added <= std_logic_vector( signed(PC_used) + signed(PC_4_8));

      PC_OFFSET <= JR_OFFSET when OPCODE = "1100111" else JB_OFFSET; -- when JALR

      PC1 <= PC_OFFSET when ((J_type = '1') or OPCODE = "1100111" or (B_type = '1' and INST(12) = '1')) and next_4 = '0' else PC_added;
      PC2 <=  PC_added when (b_or_j = '0' and OPCODE /= "1100111" ) or (B_type = '1' and INST(12) = '1') or next_4 = '1' else PC_OFFSET;

      cmp_equal <= '1' when OP1_BYPASSED = OP2_BYPASSED else '0'; -- Maybe need to be optimised?

      COMPARATOR_i: comparator
      port map(
          a => OP1_BYPASSED,
          b => OP2_BYPASSED,
          unsigned_s => INST(13),
          res => cmp_lt,
          vdd => vdd,
          vss => vss);

      cmp_for_pc <= cmp_lt when INST(14) = '1' else cmp_equal;
      NEXT_NEXT_PC_internal <= PC2 when cmp_for_pc = '1' else PC1;

      branch_taken <= '1' when (INST(12) = '0' and cmp_for_pc = '1') or (INST(12) = '1' and cmp_for_pc = '0') else '0';


    -- Registers

      REG_i: reg
      port map(
          R1_ADR => R1_ADR,
          R1_valid => R1_valid,
          REG_R1 => REG_R1,

          R2_ADR => R2_ADR,
          R2_valid => R2_valid,
          REG_R2 => REG_R2,

          INVAL_ADR => INVAL_ADR,
          INVAL_REQ => INVAL_REQ,
          INVAL_VALIDITY => inval_validity_reg,

          RD_W => RD_W,
          W_ENABLE => W_ENABLE,
          RES_W => RES_W,

          ck => ck,
          reset_n => reset_n,
          vdd => vdd,
          vss => vss);

      INVAL_VALIDITY <= inval_validity_reg when INVAL_REQ = '1' else '1';
      R1_ADR <= "00000" when (OPCODE = "0110111") else INST(19 downto 15); -- when LUI, we just copy upper bit, so R1 take 0
      R2_ADR <= INST(24 downto 20);
      INVAL_ADR <= INST(11 downto 7);
      INVAL_REQ <= '1' when B_type = '0' and S_type = '0' and nop = '0' and next_4 = '0' and operv = '1' and dec2exe_full = '0' and if2dec_empty = '0' and pushed_to_exe = '0' else '0'; -- Inst always have value to write, expecte when they store or branch or nop ; dont do it when we flush branch and do it only when we will push to exe

      operv <= '1' when (
                          (
                            ((R1_valid = '1' or BYPASS_1_B = '1') and (B_or_J = '1' or OPCODE = "1100111")) -- Valid if Branch or Jump register and R1 valid or bypass_B
                            or
                            ((R1_valid = '1' or BYPASS_1_B = '1' or BYPASS_1_A_internal = '1') and (B_or_J = '0' and OPCODE /= "1100111")) -- Valid for any instructions (except Branch or jalr) if R1 valid or BYPASS_A or BYPASS_B
                          )
                          and
                          (
                            ((R2_valid = '1' or BYPASS_2_B= '1') and B_type = '1') -- Valid if Branch et R2 valid or bypass_B
                            or
                            ((R2_valid = '1' or BYPASS_2_B= '1' or BYPASS_2_A_internal = '1') and R_type = '1') -- Valid if we use R2 register-register) and R2 valid or BYPASS_A or BYPASS_B
                            or
                            ((R2_valid = '1' or BYPASS_2_B= '1' or BYPASS_STORE_A_internal = '1') and S_type = '1') -- Valid if we use R2 Store and R2 valid or BYPASS_STORE or BYPASS_B
                            or
                            (I_type = '1' or U_type = '1') -- When I type or U type, this is valid
                          )
                        )
                        or
                        (J_type = '1') -- Always if JAL
                        or
                        (nop = '1') -- Always if nop
                        else '0';

    -- Bypass
      -- Bypass condition
        -- At start of EXE
        BYPASS_1_A_internal <= '1' when (
                                  (INST(19 downto 15) = BYPASS_RD_WHEN_EXE and OPCODE /= "0110111") or -- all inst except lui
                                  (INST(11 downto 7) = BYPASS_RD_WHEN_EXE and OPCODE = "0110111") -- For lui
                               )
                              and
                              (
                                (INST(19 downto 15) /= "00000" and OPCODE /= "0110111") or -- Reg should not be 0
                                (INST(11 downto 7) /= "00000" and OPCODE = "0110111")
                              )
                              and
                              (OPCODE /= "0010111" and J_type /= '1' and OPCODE /= "1100111") -- And not when OP1 come from PC
                              and
                              BYPASS_RD_WHEN_EXE_VALID = '1'
                              else '0';
        BYPASS_2_A_internal <= '1' when (INST(24 downto 20) = BYPASS_RD_WHEN_EXE)
                              and
                              (INST(24 downto 20) /= "00000")
                              and
                              ( I_type = '0' and U_type = '0' and S_type = '0') -- And it's not U type (lui,auipc) or I type, and not a store
                              and
                              (BYPASS_RD_WHEN_EXE_VALID = '1')
                              else '0';

        BYPASS_STORE_A_internal <= '1' when (INST(24 downto 20) = BYPASS_RD_WHEN_EXE)
                              and
                              (INST(24 downto 20) /= "00000")
                              and
                              S_type = '1' -- When it's a store
                              and
                              (BYPASS_RD_WHEN_EXE_VALID = '1')
                              else '0';


        -- At start of DEC
        BYPASS_1_B <= '1' when (
                                  (INST(19 downto 15) = BYPASS_RD_ON_DEC and OPCODE /= "0110111") or
                                  (INST(11 downto 7) = BYPASS_RD_ON_DEC and OPCODE = "0110111")
                                )
                                and
                                (
                                  (INST(19 downto 15) /= "00000" and OPCODE /= "0110111") or
                                  (INST(11 downto 7) /= "00000" and OPCODE = "0110111")
                                )
                                and
                                (BYPASS_RD_ON_DEC_VALID = '1')
                                else '0';
        BYPASS_2_B <= '1' when (INST(24 downto 20) = BYPASS_RD_ON_DEC)
                               and
                               (INST(24 downto 20) /= "00000")
                               and
                               (BYPASS_RD_ON_DEC_VALID = '1')
                               else '0';

        BYPASS_1_A <= BYPASS_1_A_internal;
        BYPASS_2_A <= BYPASS_2_A_internal;
        BYPASS_STORE_A <= BYPASS_STORE_A_internal;
      -- Bypass val
        OP1_BYPASSED <= BYPASS_VAL when BYPASS_1_B = '1' else REG_R1;
        OP2_BYPASSED <= BYPASS_VAL when BYPASS_2_B = '1' else REG_R2;

      -- Bypass save val, of inst we will push
       NEW_BYPASS_RD_VALID <= '1' when B_type = '0' and S_type = '0' and nop = '0' and OPCODE /= "0000011" and INVAL_VALIDITY = '1' else '0'; -- When result produice in EXE
       NEW_BYPASS_RD <= INST(11 downto 7);

    -- To EXE
      OP1 <= PC when OPCODE = "0010111" or J_type = '1' or OPCODE = "1100111" else OP1_BYPASSED; -- PC when auipc or link of jal(r)

      OP2 <= IMMEDIAT when OPCODE = "0110111" or OPCODE = "0010111" or OPCODE = "0000011" or OPCODE = "0100011" or OPCODE = "0010011" else --lui, auipc, l*, s*, *i
             X"00000004" when  OPCODE = "1101111" or OPCODE = "1100111" else --jal, jalr
             OP2_BYPASSED;



      STORE <= OP2_BYPASSED ;
      RD <= INST(11 downto 7);


      INST_12 <= INST(12);
      INST_13 <= INST(13);
      INST_14 <= INST(14);
      INST_30 <= INST(30);
      SUB <= '1' when (INST(30) = '1') and (OPCODE = "0110011") else '0'; -- sub

      ADD_SHIFT_or_OTHERS <= '1' when -- We do logic operation or cmp then
                  ((OPCODE = "0010011") or (OPCODE = "0110011")) -- inst is R or some I type
                  and
                  ((INST(14 downto 13) /= "00") and (INST(14 downto 12) /= "101")) -- and it's not a add or sub or shift
              else '0';
      ADD_or_SHIFT <= '1' when -- We do shift when
                (OPCODE = "0010011" or OPCODE = "0110011") -- Is I or R
                and
                (INST(12) = '1') -- and when SRL, SRA or SLL
              else '0';

      MEM_W_NOT_R <= INST(5) when MEM_OP_internal = '1' else '0';
      MEM_OP_internal <= '1' when OPCODE = "0000011" or OPCODE = "0100011" else '0'; -- This is a memory operation
      MEM_OP <= MEM_OP_internal;
      MEM_W <= '1' when MEM_OP_internal = '1' and INST(14 downto 12) = "010" else '0';
      MEM_H_B <= '1' when MEM_OP_internal = '1' and INST(12) = '1' else '0';
      MEM_S_U <= '1' when MEM_OP_internal = '1' and INST(14) = '1' else '0';




--NEXT_NEXT_PC <= X"00010074" when after_first_inst_send = '0'  else NEXT_NEXT_PC_internal; -- NEXT_NEXT_PC is start address at first, when it's a value we calculate
NEXT_NEXT_PC <= X"08000000" when after_first_inst_send = '0'  else NEXT_NEXT_PC_internal; -- NEXT_NEXT_PC is start address at first, when it's a value we calculate

process (ck)
begin
if (rising_edge(ck)) then
	if (reset_n = '0') then
		cur_state <= FETCH;
    NEXT_PC <= X"07FFFFFC";
    BYPASS_RD_WHEN_EXE_VALID <= '0' ;
    BYPASS_RD_WHEN_EXE <= BYPASS_RD_WHEN_EXE ;
    BYPASS_RD_ON_DEC_VALID <= '0' ;
    BYPASS_RD_ON_DEC <= BYPASS_RD_ON_DEC ;
    pushed_to_exe <= '0';
	else
    if get_next_inst = '1' then
  		cur_state <= next_state;
      NEXT_PC <= NEXT_NEXT_PC_internal;
      pushed_to_exe <= '0';
    else
  		cur_state <= cur_state;
      NEXT_PC <= NEXT_PC;
      if pushed_to_exe = '0' and dec2exe_push_internal = '1' then -- register we push to exe
        pushed_to_exe <= '1';
      else
        pushed_to_exe <= pushed_to_exe;
      end if;
    end if;

     -- Bypass

    if dec2exe_full = '0' and dec2exe_push_internal = '1' then -- All bypass are shifted
      if BYPASS_RD_WHEN_EXE /= NEW_BYPASS_RD then -- Bypass for DEC is only valid if the same rd will not be calculate in EXE (by instruction we juste push)
        BYPASS_RD_ON_DEC_VALID <= BYPASS_RD_WHEN_EXE_VALID ;
        BYPASS_RD_ON_DEC <= BYPASS_RD_WHEN_EXE ;
      else
        BYPASS_RD_ON_DEC_VALID <= '0' ;
        BYPASS_RD_ON_DEC <= BYPASS_RD_WHEN_EXE ;
      end if;
      BYPASS_RD_WHEN_EXE_VALID <= NEW_BYPASS_RD_VALID ;
      BYPASS_RD_WHEN_EXE <= NEW_BYPASS_RD ;
    elsif dec2exe_full = '0' then -- If EXE has push to MEM, move info about what is inside exe2mem
      BYPASS_RD_ON_DEC_VALID <= BYPASS_RD_WHEN_EXE_VALID ;
      BYPASS_RD_ON_DEC <= BYPASS_RD_WHEN_EXE ;
    elsif dec2exe_push_internal = '1' then -- If we push, then register your new RD, BYPASS_RD_WHEN_EXE is what we will have when we are in exe
      BYPASS_RD_WHEN_EXE_VALID <= NEW_BYPASS_RD_VALID ;
      BYPASS_RD_WHEN_EXE <= NEW_BYPASS_RD ;
    else
      BYPASS_RD_WHEN_EXE_VALID <= BYPASS_RD_WHEN_EXE_VALID ;
      BYPASS_RD_WHEN_EXE <= BYPASS_RD_WHEN_EXE ;
      BYPASS_RD_ON_DEC_VALID <= BYPASS_RD_ON_DEC_VALID ;
      BYPASS_RD_ON_DEC <= BYPASS_RD_ON_DEC ;
    end if;
	end if;
end if;
end process;

dec2exe_push <= dec2exe_push_internal;

dec2exe_push_internal <= '1' when (dec2exe_full = '0' and if2dec_empty = '0' and b_type = '0' and nop = '0' and next_4 = '0' and operv = '1' and inval_validity = '1') and pushed_to_exe = '0' else '0';
no_push_to_exe_require <= '1' when b_type = '1' or nop = '1' or next_4 = '1' else '0';

get_next_inst <= '1' when
                      ((pushed_to_exe = '1' or dec2exe_push_internal = '1') or no_push_to_exe_require = '1') 
                      and
                      ((fetch_ok = '1') or after_first_inst_send = '0') -- We have fetch something or just start proc (no inst send)
                      and
                      (if2dec_empty = '0' or after_first_inst = '0') -- we have a inst in dec, or we just start proc (no inst executed)
                      and
                      (
                       ((b_type = '1' or OPCODE = "1100111" ) and operv = '1') -- We wait for operv when branch or jalr
                       or
                       b_type = '0'
                       or
                       next_4 = '1'
                      )
                      else '0'; -- push to ifetch and pop from ifetch when : we have do we we have to do (push to exe) and we are ready to push new adress to ifetch (operv ok si b_type (or jalr), ok if next 4, ok sinon, and ifetch finish

dec2if_push <= get_next_inst;
if2dec_pop <= get_next_inst;


process (cur_state, j_type, OPCODE, b_type, branch_taken) 
begin
	case cur_state is
	when RUN =>
    next_4 <= '0'; -- PC + 8
    after_first_inst <= '1';
    after_first_inst_send <= '1';
    if (j_type = '1' or OPCODE = "1100111") or (b_type = '1' and branch_taken = '1') then
      next_state <= BRANCH;
    else
      next_state <= RUN;
    end if;
  when BRANCH =>
    next_4 <= '1'; -- NEXT_PC + 4
    after_first_inst <= '1';
    after_first_inst_send <= '1';
    next_state <= RUN;
  when FETCH =>
    next_4 <= '1'; -- NEXT_PC + 4
    after_first_inst <= '0';
    after_first_inst_send <= '0';
    next_state <= FETCH_START;
  when FETCH_START =>
    next_4 <= '1'; -- NEXT_PC + 4
    after_first_inst <= '0';
    after_first_inst_send <= '1';
    next_state <= RUN;
  end case;
end process;
end Behavioral;
