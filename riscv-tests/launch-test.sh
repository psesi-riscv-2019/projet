nb_test=0
nb_test_ok=0
nb_test_ko=0
total_cpi=0

for file in $(find riscv-tests/isa/bin/ -type f ! -name "*.*" -name "rv32ui-p-*")
do
    test_name=$(basename $file)
    if [[ -f $file ]] && ! [[ $test_name =~ .*fence.* ]]; then
#        echo $test_name
        ../Simu/riscv_softsimu $file --wave=wave/${test_name}.ghw &> /dev/null
        exit_code=$?
#        echo "../Simu/riscv_softsimu $file --wave=wave/${test_name}.ghw"
        nb_test=$(( nb_test + 1))
        if [ $exit_code -eq 0 ]
        then
            cpi=$(cpi_ghw/cpi wave/${test_name}.ghw)
            cur_cpi=$(echo "$cpi" | sed 's/CPI \([^ ]*\) .*/\1/')
#            total_cpi=$(python -c "print(${total_cpi}+${cur_cpi})") #cant do float operation in bash :/
            total_cpi=$( bc -l <<<  ${total_cpi}+${cur_cpi})
            echo -e "$test_name is \033[32mOK\033[0m $cpi"
            nb_test_ok=$(( nb_test_ok + 1))
            rm wave/${test_name}.ghw
        else
            echo -e "$test_name is \033[31mKO\033[0m"
            nb_test_ko=$(( nb_test_ko + 1))
        fi
    fi
done
echo -e "NB test $nb_test ( $nb_test_ok PASS, $nb_test_ko FAIL) CPI moyen $(bc -l <<<  ${total_cpi}/${nb_test_ok} | sed 's/^\([^\.]*.[0-9][0-9]\).*/\1/' )"


