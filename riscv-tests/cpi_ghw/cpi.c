#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ghwlib.h"

int main(int argc, char ** argv){

  if(argc < 2){
    printf("CPI take ghw file as argument\n");
    return EXIT_FAILURE;
  }

  struct ghw_handler h;
  struct ghw_handler *hp = &h;
  hp->flag_verbose=0;

  if (ghw_open (hp, argv[1]) != 0){
    printf("CPI could not open your file %s with ghwlib\n", argv[1]);
    return EXIT_FAILURE;
  }
  //ghw_disp_types (hp);


  if (ghw_read_base (hp) < 0){
    printf("PB when read ghw file\n");
    return EXIT_FAILURE;
  }

  // Search for our signals
  struct ghw_hie *hie = hp->hie;
  struct ghw_hie *next;

  unsigned int pop_from_dec = GHW_NO_SIG;
  unsigned int cur_state_dec = GHW_NO_SIG;
  unsigned int ck_dec = GHW_NO_SIG;
  int all_read = 0;

  while (1){
    switch (hie->kind){
      case ghw_hie_signal:
        if(strcmp(hie->name,"if2dec_pop") == 0){
//          printf ("%s #%u \n",hie->name, hie->u.sig.sigs[0]);
          pop_from_dec = hie->u.sig.sigs[0];
        }
        if((strcmp(hie->name,"cur_state") == 0) && (strcmp(hie->parent->name,"decode_stage") == 0)){
//          printf ("%s #%u (parent %s)\n",hie->name, hie->u.sig.sigs[0], hie->parent->name);
          cur_state_dec = hie->u.sig.sigs[0];
        }
        if((strcmp(hie->name,"ck") == 0)){ // && (strcmp(hie->parent->name,"decode_stage") == 0)){
//          printf ("%s #%u (parent %s)\n",hie->name, hie->u.sig.sigs[0], hie->parent->name);
          ck_dec = hie->u.sig.sigs[0];
        }
        next = hie->brother;
        break;
      case ghw_hie_port_in:
      case ghw_hie_port_out:
      case ghw_hie_port_inout:
      case ghw_hie_port_buffer:
      case ghw_hie_port_linkage:
        next = hie->brother;
        break;
      case ghw_hie_design:
      case ghw_hie_block:
      case ghw_hie_generate_if:
      case ghw_hie_generate_for:
      case ghw_hie_instance:
      case ghw_hie_process:
      case ghw_hie_package:
        next = hie->u.blk.child;
        if (next == NULL) {
          next = hie->brother;
        }
        break;
      default:
        abort();
    }

    while (next == NULL){
      if (hie->parent == NULL){
        all_read = 1;
        break;
      }
      hie = hie->parent;
      next = hie->brother;
    }
    if(all_read){
      break;
    }else{
      hie = next;
    }
  }

  if ((pop_from_dec == GHW_NO_SIG) || (cur_state_dec == GHW_NO_SIG) || (ck_dec == GHW_NO_SIG)){
    printf("pop_from_dec or cur_state_dec signal or ck not found\n");
    return EXIT_FAILURE;
  }

  // For all signals 
  unsigned int inst_number = 0;
  unsigned int cycles = 0;



  enum ghw_sm_type sm = ghw_sm_init;
  int eof = 0;
  unsigned i;
  union ghw_val *ck_val;
  union ghw_type *ck_type;
  union ghw_val *pop_val;
  union ghw_type *pop_type;
  union ghw_val *cur_state_val;
  union ghw_type *cur_state_type;
  enum ghdl_rtik pop_enum;
  while (!eof){
    switch (ghw_read_sm (hp, &sm)){
      case ghw_res_snapshot:
      case ghw_res_cycle:
          ck_val = hp->sigs[ck_dec].val;
          ck_type = hp->sigs[ck_dec].type;
//          printf("ck %s\n",ck_type->en.lits[ck_val->e8]);

          pop_val = hp->sigs[pop_from_dec].val;
          pop_type = hp->sigs[pop_from_dec].type;
//          printf("pop_from_dec %s\n",pop_type->en.lits[pop_val->e8]);

          cur_state_val = hp->sigs[cur_state_dec].val;
          cur_state_type = hp->sigs[cur_state_dec].type;
//          printf("cur_state  %s\n",cur_state_type->en.lits[cur_state_val->e8]);
          if((strcmp(pop_type->en.lits[pop_val->e8], "'1'") == 0) && (strcmp(cur_state_type->en.lits[cur_state_val->e8], "run") == 0) && (strcmp(ck_type->en.lits[ck_val->e8], "'1'") == 0)){
//            printf("New inst\n");
            inst_number++;
          }
        break;
      case ghw_res_eof:
        eof = 1;
        break;
      default:
        abort ();
    }
  }
//  printf("End time %lld\n",hp->snap_time);
  cycles = hp->snap_time / 1000000 / 2; // 1 cycle is 2 ns;
  float cpi = (float) cycles / inst_number;
  printf("CPI %lg (cycles=%d inst_executed=%d)\n",cpi,cycles,inst_number);
}

